/**
 * Created by leand on 14.12.2016.
 */
// Diesen Code habe ich mithilfe dieses Beispiels geschrieben: http://bl.ocks.org/mbostock/4063663


var color = d3.scaleOrdinal(d3.schemeCategory10);


d3.csv("oliveoil.csv", function(error, data) {
   if (error) throw error;
   data.forEach(function(d){
       d.palmitic = +d.palmitic;
       d.palmitoleic = +d.palmitoleic;
       d.stearic = +d.stearic;
       d.oleic = +d.oleic;
       d.linoleic = +d.linoleic;
       d.linolenic = +d.linolenic;
       d.arachidic = +d.arachidic;
       d.eicosenoic = +d.eicosenoic;
   });
   var oils = data.columns.filter(function(d) { return d !== "Region" && d !== "Area"});
   var n = oils.length;
    var domainByOil = {};

    var margin =  {top:20,right:100,bottom:20,left:30},
        width = 1240-margin.left - margin.right,
        height = 1240 - margin.top - margin.bottom,
        padding = 10,
        size = (width)/n;

    var xScale = d3.scaleLinear()
        .range([padding / 2, size - padding / 2]);

    var yScale = d3.scaleLinear()
        .range([size - padding / 2, padding / 2]);

    var xAxis = d3.axisBottom(xScale)
        .ticks(6);

    var yAxis = d3.axisLeft(yScale)
        .ticks(6);

   console.log("Oils:", oils);
   console.log(data);

   oils.forEach(function(oil) {
      domainByOil[oil] = d3.extent(data, function(d) { return d[oil]; });
   });

    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + ( margin.left) + "," + margin.top + ")");

    svg.selectAll(".x.axis")
        .data(oils)
        .enter().append("g")
        .attr("class", "x axis")
        .attr("transform", function(d, i) { return "translate(" + (n - i - 1) * size + "," +(width)+ ")"; })
        .each(function(d) { xScale.domain(domainByOil[d]); d3.select(this).call(xAxis); });

    svg.selectAll(".y.axis")
        .data(oils)
        .enter().append("g")
        .attr("class", "y axis")
        .attr("transform", function(d, i) { return "translate(0," + i * size + ")"; })
        .each(function(d) { yScale.domain(domainByOil[d]); d3.select(this).call(yAxis); });

    var cell = svg.selectAll(".cell")
        .data(cross(oils, oils))
        .enter().append("g")
        .attr("class", "cell")
        .attr("transform", function(d) { return "translate(" + (n - d.i - 1) * size + "," + d.j * size + ")"; })
        .each(plot);

    // Titles for the diagonal.
    cell.filter(function(d) { return d.i === d.j; }).append("text")
        .attr("x", padding)
        .attr("y", padding)
        .attr("dy", ".71em")
        .text(function(d) { return d.x; });
     var isAreaGrey = {};
     color.domain().forEach(function(d) {
        isAreaGrey[d] = false;
     });
    // draw legend
    var legend = svg.selectAll(".legend")
        .data(color.domain())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; })
        .on("click",function(d) {
            isAreaGrey[d] = !isAreaGrey[d];
            console.log("onclick",d);
            svg.selectAll("circle").style("fill", function(d2){


               return isAreaGrey[d2.Area] ? "#bbb" : color(d2.Area);
            }).style("opacity", function(d2){


                return isAreaGrey[d2.Area] ? "0.4" : "0.7";
            });
            legend.selectAll("rect")
                .style("fill",function(d){
                   return isAreaGrey[d] ? "#bbb" : color(d);
                });
        });

    // draw legend colored rectangles
    legend.append("rect")
        .attr("x", width + margin.right + margin.left - 18 - 40)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

    // draw legend text
    legend.append("text")
        .attr("x", width + margin.right + margin.left - 24 - 40)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .style("fill","000")
        .text(function(d) { return d;});


    function plot(p) {

        var cell = d3.select(this);

        xScale.domain(domainByOil[p.x]);
        yScale.domain(domainByOil[p.y]);

        cell.append("rect")
            .attr("class", "frame")
            .attr("x", padding / 2)
            .attr("y", padding / 2)
            .attr("width", size - padding)
            .attr("height", size - padding);

        cell.selectAll("circle")
            .data(data)
            .enter().append("circle")
            .attr("cx", function(d) { return xScale(d[p.x]); })
            .attr("cy", function(d) { return yScale(d[p.y]); })
            .attr("r", 4)
            .style("fill", function(d) { return color(d.Area); });
    }
});





function cross(a, b) {
    var c = [], n = a.length, m = b.length, i, j;
    for (i = -1; ++i < n;) for (j = -1; ++j < m;) c.push({x: a[i], i: i, y: b[j], j: j});
    return c;
}